import cv2
import numpy as np

FileName = '2'
VideoPath = FileName + '.mp4'
cap = cv2.VideoCapture(VideoPath)

# Check if camera opened successfully
if (cap.isOpened() == False):
  print("Error opening video stream or file")
count = 1
# Read until video is completed
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
    path = 'img/image' + FileName + '-' + str(count) + '.png'
    print(path)
    count += 1
    # Display the resulting frame
    frame = cv2. rotate(frame, cv2.ROTATE_90_CLOCKWISE)
    # cv2.imshow('Frame',frame)
    cv2.imwrite(path, frame)

    ## Press Q on keyboard to  exit
    # if cv2.waitKey(25) & 0xFF == ord('q'):
    #       break

  # Break the loop
  else:
    break

# When everything done, release the video capture object
cap.release()

# Closes all the frames
cv2.destroyAllWindows()
